
module app;

import std.algorithm;
import std.file;

import cwx.binary;
import cwx.imagesize;

import org.eclipse.swt.all;

import java.lang.all : ArrayWrapperString2;
import java.io.ByteArrayInputStream;

void main(string[] args) {
	auto display = new Display;

	Shell boot(string file) {
		auto shell = new Shell;
		shell.setText("格納ビットマップ修復 for CardWirth 1.50-1.60");
		shell.setLayout(new FillLayout);
		shell.setSize(new Point(600.ppis, 400.ppis));

		auto fileName = "";
		Label img1 = null;
		Label img2 = null;
		Label img3 = null;
		Image i1 = null;
		Image i2 = null;
		Image i3 = null;
		Button b1 = null;
		Button b2 = null;
		bool load(string file) {
			uint x, y;
			if (!.bmpSize(file, x, y)) return false;
			fileName = file;
			void load2(ref Image img, Label label, bool fix1, bool fix2) {
				auto imgData = .readImage(file, fix1, fix2);
				if (img) img.dispose();
				img = imgData ? new Image(display, imgData) : null;
				if (!img) {
					auto gc = new GC(label);
					auto t = "破損したイメージ";
					auto te = gc.textExtent(t);
					gc.dispose();
					img = new Image(display, te.x, te.y);
					gc = new GC(img);
					gc.setFont(label.getFont());
					gc.setBackground(label.getBackground());
					gc.fillRectangle(0, 0, te.x, te.y);
					gc.drawText(t, 0, 0);
					gc.dispose();
				}
				label.setImage(img);
			}
			load2(i1, img1, false, false);
			load2(i2, img2, true, false);
			load2(i3, img3, false, true);
			b1.setEnabled(i2 !is null);
			b2.setEnabled(i3 !is null);
			return true;
		}
		void save(Image img) {
			if (!img) return;
			auto dlg = new FileDialog(shell, SWT.APPLICATION_MODAL | SWT.SINGLE | SWT.SAVE);
			dlg.setText("修復されたイメージの保存");
			dlg.setFilterExtensions(["*.bmp", "*.png"]);
			dlg.setFilterNames(["ビットマップイメージ (*.bmp)", "PNGイメージ (*.png)"]);
			dlg.setFileName(fileName);
			dlg.setOverwrite(true);
			auto r = dlg.open();
			if (r == "") return;
			auto loader = new ImageLoader;
			loader.data = [img.getImageData()];
			switch (dlg.getFilterIndex()) {
			case 0:
				loader.save(r, SWT.IMAGE_BMP);
				break;
			case 1:
				loader.save(r, SWT.IMAGE_PNG);
				break;
			default:
				break;
			}
		}

		auto sash1 = new SashForm(shell, SWT.HORIZONTAL | SWT.BORDER);

		auto compL = new Composite(sash1, SWT.NONE);
		compL.setLayout(new GridLayout(1, false));
		auto l = new Label(compL, SWT.WRAP);
		l.setText("ここに壊れたビットマップイメージ(*.bmp)をドロップ");
		img1 = new Label(compL, SWT.CENTER);
		img1.setLayoutData(new GridData(GridData.FILL_BOTH));

		auto sash2 = new SashForm(sash1, SWT.VERTICAL | SWT.BORDER);

		Label fixedImagePanel(string name, out Button b) {
			auto comp = new Composite(sash2, SWT.NONE);
			comp.setLayout(new GridLayout(1, false));
	 		auto l = new Label(comp, SWT.WRAP);
	 		l.setText(name);
			auto img = new Label(comp, SWT.CENTER);
			img.setLayoutData(new GridData(GridData.FILL_BOTH));
			b = new Button(comp, SWT.NONE);
			b.setText("名前を付けて保存...");
			b.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
			b.setEnabled(false);
			b.addSelectionListener(new class SelectionAdapter {
				override void widgetSelected(SelectionEvent e) {
					save(img.getImage());
				}
			});
			return img;
		}
		img2 = fixedImagePanel("ピクセルデータ開始位置の修復", b1);
		img3 = fixedImagePanel("不正なパレット情報の修復", b2);

		auto dt = new DropTarget(shell, DND.DROP_LINK);
		dt.setTransfer([FileTransfer.getInstance()]);
		dt.addDropListener(new class DropTargetAdapter {
			override void dragEnter(DropTargetEvent e) {
				e.detail = DND.DROP_LINK;
			}
			override void drop(DropTargetEvent e) {
				auto files = (cast(ArrayWrapperString2)e.data).array;
				auto booted = false;
				foreach (file; files) {
					if (booted) {
						uint x, y;
						if (.bmpSize(file, x, y)) boot(file);
					} else if (load(file)) {
						booted = true;
					}
				}
			}
		});

		if (file != "") {
			load(file);
		}

		shell.addDisposeListener(new class DisposeListener {
			override void widgetDisposed(DisposeEvent e) {
				if (i1) i1.dispose();
				if (i2) i2.dispose();
				if (i3) i3.dispose();
			}
		});

		shell.open();
		return shell;
	}

	Shell[] shells;
	auto booted = false;
	foreach (file; args[1 .. $]) {
		uint x, y;
		if (.bmpSize(file, x, y)) {
			shells ~= boot(file);
			booted = true;
		}
	}
	if (!booted) shells ~= boot("");

	do {
		if (!display.readAndDispatch()) {
			display.sleep();
		}
	} while (display.getShells().length);
	display.dispose();
}

ImageData readImage(string path, bool fix1, bool fix2) {
	auto bytes = cast(byte[])std.file.read(path);
	if (fix1) {
		// データ開始位置が壊れているバグへの対処
		.fixCWNext16BitBitmap(bytes);
	}
	if (fix2) {
		// 格納された32-bitビットマップにパレットが残留しているバグへの対処
		.fixCWNext32BitBitmap(bytes);
		.fixCWNext16BitBitmap(bytes);
	}
	try {
		auto s = new ByteArrayInputStream(bytes);
		scope (exit) s.close();
		auto data = new ImageData(s);
		if (32 == data.depth && 'B' == bytes[0] && 'M' == bytes[1]) {
			// CardWirthのデコーダは予備色をアルファ値として扱うが、
			// 1件も0以外の数値が無い場合は扱わない
			foreach (y; 0 .. data.height) {
				foreach (x; 0 .. data.width) {
					auto b = cast(ubyte)data.data[y * data.bytesPerLine + x * 4 + 3];
					if (b != 0) {
						if (!data.alphaData) data.alphaData = new byte[data.width * data.height];
						data.alphaData[y * data.width + x] = b;
					}
				}
			}
		}
		uint r, g, b;
		if (.getBitField(bytes, r, g, b) && data.depth == 32 && data.palette) {
			// BUG: このケースでビットフィールドの設定がおかしくなる
			if (r == 0x00ff0000 && g == 0x0000ff00 && b == 0x000000ff) {
				data.palette.redMask = b << 8;
				data.palette.greenMask = g << 8;
				data.palette.blueMask = r << 8;
			}
		}
		return data;
	} catch (Throwable e) {
		return null;
	}
}

@property
int ppis(int value) {
	return value * dpiMuls;
}

@property
int dpiMuls() {
	auto d = Display.getCurrent();
	auto dpi = d.getDPI().x;
	immutable base = 96;
	auto exp = 2;
	while (base * exp <= dpi) {
		exp *= 2;
	}
	return exp / 2;
}
